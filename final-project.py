import itertools
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import NullFormatter
import pandas as pd
import numpy as np
import matplotlib.ticker as ticker
from sklearn import preprocessing

# This dataset is about the performance of basketball teams. The cbb.csv data set includes performance data about five seasons of 354 basketball teams
df = pd.read_csv('https://s3-api.us-geo.objectstorage.softlayer.net/cf-courses-data/CognitiveClass/ML0120ENv3/Dataset/ML0101EN_EDX_skill_up/cbb.csv')

# add a column that will contain "true" if the wins above bubble are over 7 and "false" if not. We'll call this column Win Index or "windex" for short.
df['windex'] = np.where(df.WAB > 7, 'True', 'False')

# Next we'll filter the data set to the teams that made the Sweet Sixteen, the Elite Eight, and the Final Four in the post season. We'll also create a new dataframe that will hold the values with the new column.
df1 = df[df['POSTSEASON'].str.contains('F4|S16|E8', na=False)]

# plot
import seaborn as sns

bins = np.linspace(df1.BARTHAG.min(), df1.BARTHAG.max(), 10)
g = sns.FacetGrid(df1, col="windex", hue="POSTSEASON", palette="Set1", col_wrap=2)
g.map(plt.hist, 'BARTHAG', bins=bins, ec="k")

g.axes[-1].legend()
plt.show()

bins = np.linspace(df1.ADJOE.min(), df1.ADJOE.max(), 10)
g = sns.FacetGrid(df1, col="windex", hue="POSTSEASON", palette="Set1", col_wrap=2)
g.map(plt.hist, 'ADJOE', bins=bins, ec="k")

g.axes[-1].legend()
plt.show()

# Lets look at how Adjusted Defense Efficiency plots
bins = np.linspace(df1.ADJDE.min(), df1.ADJDE.max(), 10)
g = sns.FacetGrid(df1, col="windex", hue="POSTSEASON", palette="Set1", col_wrap=2)
g.map(plt.hist, 'ADJDE', bins=bins, ec="k")
g.axes[-1].legend()
plt.show()

#Lets look at the postseason
df1.groupby(['windex'])['POSTSEASON'].value_counts(normalize=True)

#Lets convert wins above bubble (winindex) under 7 to 0 and over 7 to 1:
df['windex'].replace(to_replace=['False','True'], value=[0,1],inplace=True)

# How about seed?
df1.groupby(['SEED'])['POSTSEASON'].value_counts(normalize=True)

# Feature before One Hot Encoding
df1[['ADJOE','ADJDE','BARTHAG','EFG_O','EFG_D']].head()

# Use one hot encoding technique to conver categorical varables to binary variables and append them to the feature Data Frame
Feature = df1[['ADJOE','ADJDE','BARTHAG','EFG_O','EFG_D']]
Feature = pd.concat([Feature,pd.get_dummies(df1['POSTSEASON'])], axis=1)
Feature.drop(['S16'], axis = 1,inplace=True)
Feature.head()

# Feature selection
X = Feature
# What are our lables? Round where the given team was eliminated or where their season ended (R68 = First Four, R64 = Round of 64, R32 = Round of 32, S16 = Sweet Sixteen, E8 = Elite Eight, F4 = Final Four, 2ND = Runner-up, Champion = Winner of the NCAA March Madness Tournament for that given year)|
y = df1['POSTSEASON'].values

# Normalize Data
X= preprocessing.StandardScaler().fit(X).transform(X)

#Training and Validation

# We split the X into train and test to find the best k
from sklearn.model_selection import train_test_split
X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=0.2, random_state=4)
print ('Train set:', X_train.shape,  y_train.shape)
print ('Validation set:', X_val.shape,  y_val.shape)

# K Nearest Neighbor(KNN)

from sklearn.metrics import accuracy_score
from sklearn.neighbors import KNeighborsClassifier

# set k
k=3
# Train Model and Predict
knn_model = KNeighborsClassifier(n_neighbors = k).fit(X_train,y_train)

knn_yhat = knn_model.predict(X_val)

# evaluation
print("Test set Accuracy: ", accuracy_score(y_val, knn_yhat))

# many Ks
Ks = np.linspace(1,15,15,dtype=int)
score = np.zeros(Ks.size)

for k in Ks:
    # train and predict
    model = KNeighborsClassifier(n_neighbors = k).fit(X_train,y_train)
    yhat = model.predict(X_val)
    score[k-1] = accuracy_score(y_val, yhat)
    print("Test sample KNN accuracy score for k={}: {:.3f}".format(k, float(score[k-1])))

# plot
plt.plot(range(1,Ks),score,'g')
plt.legend(('Accuracy ', '+/- 3xstd'))
plt.ylabel('Accuracy ')
plt.xlabel('Number of Neighbors (K)')
plt.tight_layout()
plt.show()

# decision tree
from sklearn.tree import DecisionTreeClassifier

dt_model = DecisionTreeClassifier(criterion="entropy", max_depth = 4).fit(X_train, y_train)
dt_yhat = dt_model.predict(X_val)

print("DecisionTrees's Accuracy: ", accuracy_score(y_val, dt_yhat))

score = 0
n = 0
while (score < dt_score):
    n=n+1
    model = DecisionTreeClassifier(criterion="entropy", max_depth = n).fit(X_train, y_train)
    yhat = model.predict(X_val)
    score = accuracy_score(y_val, yhat)
    print("DT score for max_depth={}: {:.3f}".format(n, score))

print("Minimun max_depth is {}".format(n))

#SVM
from sklearn import svm

# fit model and predict
svm_model = svm.SVC(kernel='linear').fit(X_train, y_train)
svm_yhat = svm_model.predict(X_val)

from sklearn.metrics import confusion_matrix, f1_score, jaccard_similarity_score
print("F-1 score: ", f1_score(y_val, svm_yhat, average='weighted') )
print("jaccard score: ", jaccard_similarity_score(y_val, svm_yhat) )
print("confusion matrix: \n", confusion_matrix(y_val, svm_yhat, labels=['E8','F4', 'S16']))

# logistic regression

from sklearn.linear_model import LogisticRegression
# fit and predict

lr_model = LogisticRegression(C=0.5, solver='saga').fit(X_train,y_train)
lr_yhat = lr_model.predict(X_val)
lr_yhat[0:5]

print( "jaccard score: ", jaccard_similarity_score(y_val, lr_yhat))
print("confusion matrix: \n", confusion_matrix(y_val, lr_yhat, labels=['E8','F4', 'S16']))

from sklearn.metrics import log_loss
print("log loss: ", log_loss(y_val, lr_model.predict_proba(X_val)) )
print (classification_report(y_val, lr_yhat))
